import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchCategory } from "../redux/actions/CategoryAction";
import { Container, Form, Table, Button } from "react-bootstrap";

export default function Category() {
  const [category, setCategory] = useState([]);
  const [isUpdate, setIsUpdate] = useState(null);
  const [newCategory, setNewCategory] = useState(null);

  const dispatch = useDispatch();
  const state = useSelector((state) => state.CategoryReducer.categories);

  useEffect(() => {
    dispatch(fetchCategory());
  }, []);
  return (
    <div>
      <Container>
        <h5 className="my-4">Category</h5>
        <Form>
          <Form.Group>
            <Form.Label>Category Name</Form.Label>
            <Form.Control
              id="text"
              type="text"
              placeholder="Category Name"
              onChange={(e) => setNewCategory(e.target.value)}
            />
            <br />
            <Button type="submit" variant="secondary">
              {isUpdate !== null ? "Save" : "Add"}
            </Button>
            <Form.Text className="text-muted"></Form.Text>
          </Form.Group>
        </Form>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {state.map((item, index) => (
              <tr key={index}>
                <td>{item._id.slice(0, 5)}</td>
                <td>{item.name}</td>
                <td>
                  <Button size="sm" variant="warning">
                    Edit
                  </Button>{" "}
                  <Button size="sm" variant="danger">
                    Delete
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    </div>
  );
}
