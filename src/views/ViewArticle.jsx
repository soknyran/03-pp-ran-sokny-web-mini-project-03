import React, { useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { fetchArticleById } from "../redux/actions/ArticleAction";

export default function ViewArticle() {
  let { id } = useParams();
  const dispatch = useDispatch();
  const state = useSelector((state) => state.ArticleReducer.view);

  useEffect(() => {
    dispatch(fetchArticleById(id));
  }, []);

  return (
    <Container>
      {state ? (
        <>
          <Row>
            <Col md={8}>
              <h1 className="my-2">{state.title}</h1>
              <img
                width="500"
                height="100%"
                alt="..."
                src={
                  state.image
                    ? state.image
                    : "https://wallpaperaccess.com/full/7445.jpg"
                }
              />
              <p>{state.description}</p>
            </Col>
          </Row>
        </>
      ) : (
        <h1>Loading...</h1>
      )}
    </Container>
  );
}
