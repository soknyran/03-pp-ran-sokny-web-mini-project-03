import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteAuthor,
  fetchAuthor,
  postAuthor,
  updateAuthor,
} from "../redux/actions/AuthorAction";
import { Container, Form, Row, Col, Button, Table } from "react-bootstrap";
import { bindActionCreators } from "redux";
import { uploadImage } from "../redux/actions/ArticleAction";
import Swal from "sweetalert2";

export default function Author() {
  const [imageURL, setImageURL] = useState(
    "https://designshack.net/wp-content/uploads/placeholder-image.png"
  );
  const [imageFile, setImageFile] = useState(null);
  const [name, setName] = useState(null);
  const [email, setEmail] = useState(null);
  const [isUpdate, setIsUpdate] = useState(null);
  const [isRefresh, setIsRefresh] = useState(false);
  const dispatch = useDispatch();
  const onDelete = bindActionCreators(deleteAuthor, dispatch);
  const onUploadImg = bindActionCreators(uploadImage, dispatch);
  const onPost = bindActionCreators(postAuthor, dispatch);
  const onUpdateAuthor = bindActionCreators(updateAuthor, dispatch);
  const state = useSelector((state) => state.AuthorReducer.authors);

  useEffect(() => {
    dispatch(fetchAuthor());
    setIsRefresh(false);
  }, [isRefresh]);

  const onAdd = async (e) => {
    e.preventDefault();
    let newAuthor = { name, email };

    if (imageFile) {
      let url = await onUploadImg(imageFile);
      newAuthor.image = url.payload;
    }
    onPost(newAuthor).then((message) => {
      setIsRefresh(true);
      Swal.fire({
        position: "top",
        icon: "success",
        title: message.payload,
        showConfirmButton: false,
        timer: 1500,
      });
    });
    setImageFile(null);
    clear();
  };

  function clear() {
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("img").value = "";
    document.getElementById("image").src =
      "https://designshack.net/wp-content/uploads/placeholder-image.png";
  }

  const onEdit = (id, name, email, image) => {
    setIsUpdate(id);
    setName(name);
    setEmail(email);
    document.getElementById("name").value = name;
    document.getElementById("email").value = email;
    document.getElementById("image").src = image;
  };

  const onUpdate = async (e) => {
    e.preventDefault();
    let newAuthor = { name, email };

    if (imageFile) {
      let url = await onUploadImg(imageFile);
      newAuthor.image = url.payload;
    }

    onUpdateAuthor(isUpdate, newAuthor).then((message) => {
      Swal.fire({
        position: "top",
        icon: "success",
        title: message.payload,
        showConfirmButton: false,
        timer: 1500,
      });
    });
    setIsRefresh(true);
    setIsUpdate(null);
    setImageFile(null);
    clear();
  };

  return (
    <div>
      <Container className="my-4">
        <h5>Author</h5>
        <Row>
          <Col md={9}>
            <Form>
              <Form.Group>
                <Form.Label>Auth Name</Form.Label>
                <Form.Control
                  id="name"
                  type="text"
                  placeholder="Author Name"
                  onChange={(e) => setName(e.target.value)}
                />
                <Form.Text className="text-muted"></Form.Text>
              </Form.Group>

              <Form.Group>
                <Form.Label>Email</Form.Label>
                <Form.Control
                  id="email"
                  type="text"
                  placeholder="Email"
                  onChange={(e) => setEmail(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Button
                variant="primary"
                type="submit"
                onClick={isUpdate !== null ? onUpdate : onAdd}
              >
                {isUpdate !== null ? "Save" : "Add"}
              </Button>
            </Form>
          </Col>
          <Col md={3}>
            <img id="image" className="w-100" alt="..." src={imageURL} />
            <Form>
              <Form.Group>
                <Form.File
                  id="img"
                  label="Choose Image"
                  onChange={(e) => {
                    let url = URL.createObjectURL(e.target.files[0]);
                    setImageFile(e.target.files[0]);
                    setImageURL(url);
                  }}
                />
              </Form.Group>
            </Form>
          </Col>
        </Row>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {state.map((item, index) => (
              <tr key={index}>
                <td>{item._id.slice(0, 5)}</td>
                <td>{item.name}</td>
                <td>{item.email}</td>
                <td>
                  <img src={item.image} alt="" width="auto" height={100} />
                </td>
                <td>
                  <Button
                    size="sm"
                    variant="warning"
                    onClick={() =>
                      onEdit(item._id, item.name, item.email, item.image)
                    }
                  >
                    Edit
                  </Button>{" "}
                  <Button
                    size="sm"
                    variant="danger"
                    onClick={() => onDelete(item._id)}
                  >
                    Delete
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    </div>
  );
}
