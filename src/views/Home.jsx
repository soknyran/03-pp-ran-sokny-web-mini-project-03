import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { deleteArticle, fetchArticle } from "../redux/actions/ArticleAction";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router";
import { fetchCategory } from "../redux/actions/CategoryAction";
import ReactLoading from "react-loading";

export default function Home() {
  const dispatch = useDispatch();
  const onDelete = bindActionCreators(deleteArticle, dispatch);
  const article = useSelector((state) => state.ArticleReducer.articles);
  const category = useSelector((state) => state.CategoryReducer.categories);
  const history = useHistory();
  const [isArticleLoading, setIsArticleLoading] = useState(true);
  const [isCategoryLoading, setIsCategoryLoading] = useState(true);

  useEffect(() => {
    dispatch(fetchArticle()).then(() => {
      setIsArticleLoading(false);
    });
    dispatch(fetchCategory()).then(() => {
      setIsCategoryLoading(false);
    });
  }, []);

  const Loading = () => (
    <ReactLoading
      className="loading"
    />
  );

  return (
    <Container className="my-4">
      {isArticleLoading && isCategoryLoading ? (
        Loading()
      ) : (
        <Row>
          <Col md="3">
            <Card>
              <Card.Img
                variant="top"
                style={{ objectFit: "cover", height: "250px" }}
                src="https://wallpaperaccess.com/full/7445.jpg"
              />
              <Card.Body>
                <Card.Title>Please Login</Card.Title>
                <Button variant="primary">Login With Google</Button>
              </Card.Body>
            </Card>
          </Col>
          <Col md="9">
            <h5>Category</h5>
            <Row className="my-3">
              <Col>
                {category.map((item, index) => (
                  <Button
                    key={index}
                    className="mx-1"
                    variant="outline-secondary"
                    size="sm"
                  >
                    {item.name}
                  </Button>
                ))}
              </Col>
            </Row>
            <Row className="my-3">
              {article.map((item, index) => (
                <Col key={index} md="4">
                  <Card>
                    <Card.Img
                      variant="top"
                      style={{ objectFit: "cover", height: "150px" }}
                      src={item.image}
                    />
                    <Card.Body>
                      <Card.Title>{item.title}</Card.Title>
                      <Card.Text className="text-line-3">
                        {item.description}
                      </Card.Text>
                      <Button
                        variant="primary"
                        size="sm"
                        onClick={() => history.push("/view/" + item._id)}
                      >
                        Read
                      </Button>{" "}
                      <Button
                        variant="warning"
                        size="sm"
                        onClick={() => {
                          history.push("/update/article/" + item._id);
                        }}
                      >
                        Edit
                      </Button>{" "}
                      <Button
                        variant="danger"
                        size="sm"
                        onClick={() => onDelete(item._id)}
                      >
                        Delete
                      </Button>
                    </Card.Body>
                  </Card>
                </Col>
              ))}
            </Row>
          </Col>
        </Row>
      )}
    </Container>
  );
}
