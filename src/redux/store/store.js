import { applyMiddleware, createStore, combineReducers } from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import ArticleReducer from "../reducers/ArticleReducer";
import AuthorReducer from "../reducers/AuthorReducer";
import CategoryReducer from "../reducers/CategoryReducer";

const rootReducers = combineReducers({
  ArticleReducer,
  AuthorReducer,
  CategoryReducer,
});

export const store = createStore(rootReducers, applyMiddleware(thunk, logger));
