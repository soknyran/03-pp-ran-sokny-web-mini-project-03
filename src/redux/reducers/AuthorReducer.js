const initialState = {
  authors: [],
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case "FETCH_AUTHOR":
      return { ...state, authors: payload };

    case "DELETE_AUTHOR":
      return {
        ...state,
        authors: state.authors.filter((item) => item._id !== payload._id),
      };

    case "POST_AUTHOR":
      return { ...state, payload };

    case "UPDATE_AUTHOR":
      return { ...state, payload };

    default:
      return state;
  }
};
