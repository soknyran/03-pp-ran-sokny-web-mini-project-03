const initialState = {
  categories: [],
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case "FETCH_CATEGORY":
      return { ...state, categories: payload };

    case "POST_CATEGORY":
      return { ...state, payload };

    default:
      return state;
  }
};
