import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import Menu from './components/Menu';
import Home from './views/Home';
import Article from "./views/Article";
import Author from "./views/Author";
import Category from "./views/Category";
import ViewArticle from "./views/ViewArticle";


export default function App() {
  return (
    <div>
      <Router>
        <Menu />
        <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/article' component={Article} />
          <Route path='/author' component={Author} />
          <Route path='/category' component={Category} />
          <Route path='/view/:id' component={ViewArticle} />
        </Switch>
      </Router>
    </div>
  )
}